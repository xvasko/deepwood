# Ruby on Rails - Instagram clone

School project: [PV249](https://is.muni.cz/auth/predmet/fi/podzim2016/PV249?lang=en;setlang=en) - Ruby development

Available at: http://deepwood.herokuapp.com/
(wait a few seconds for heroku dyno to wake up)

![alt](https://firebasestorage.googleapis.com/v0/b/uagoo-34302.appspot.com/o/deepwood.PNG?alt=media&token=c18f8937-62b7-404a-8309-89cdf1167706)